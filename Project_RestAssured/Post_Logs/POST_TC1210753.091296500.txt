Endpoint is :
https://jsonplaceholder.typicode.com/posts

Request body is :
{
    "firstname": "Akansha",
    "lastname": "Inda"
}

Response header date is : 
Tue, 16 Apr 2024 15:37:53 GMT

Response body is : 
{
  "firstname": "Akansha",
  "lastname": "Inda",
  "id": 101
}