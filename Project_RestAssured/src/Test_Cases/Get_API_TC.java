package Test_Cases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
public class Get_API_TC extends RequestBody {

	public static void executor() throws ClassNotFoundException, IOException {
		File dir_name = Utility.CreateLogDirectory("Get_API_Logs");
		
		String Endpoint = RequestBody.Hostname() + RequestBody.Resource2 ();
		int statuscode=0;

		for (int i = 0; i < 5; i++) {
		Response response = API_Trigger.GET_trigger(RequestBody.Headername(), RequestBody.Headervalue(),
				 Endpoint);
		statuscode = response.statusCode();

		if (statuscode == 200) {
		
		Utility.evidenceGetFile(Utility.testLogName("Get_TC1"), dir_name, Endpoint,
				response.getHeader("Date"), response.getBody().asString());
		validator(response);
		break;
	}
	else {
		System.out.println("Expected status code is not found in current iteration :" +i+ " hence retrying");
	}
}

if (statuscode!=200) {
	System.out.println("Expected status code not found even after 5 retries hence failing the test case");
	Assert.assertEquals(statuscode, 201);
}
}

	public static void validator(Response response) {

			String res_body = response.getBody().asString();
			System.out.println(response.getBody().asString());
			int statusCode = response.getStatusCode();
			System.out.println("statusCode is:" + " " + statusCode);

			JsonPath res_jsn = new JsonPath(res_body);
			int count = res_jsn.getInt("data.size()");
			System.out.println("count of data array from response:" + " " + count);



int id[] = { 1,2,3,4,5 };
String name[] = {"id labore ex et quam laborum","quo vero reiciendis velit similique earum","odio adipisci rerum aut animi","alias odio sit","vero eaque aliquid doloribus et culpa" };
String email[] = { "Eliseo@gardner.biz","Jayne_Kuhic@sydney.com","Nikita@garfield.biz","Lew@alysha.tv","Hayden@althea.biz" };


  
int id_Arr[] = new int[count];
String email_Arr[] = new String[count];
String name_Arr[] = new String[count];


for (int i = 0; i < count-1; i++) {
	{System.out.println("\r\n" + "-fetched array data from arrays of expected data-");
	
		int res_id = res_jsn.getInt("[" + i + "].id");
		System.out.println("r\n" + res_id);
		id_Arr[i] = res_id;

		String res_Email = res_jsn.get("[" + i + "].email");
		System.out.println(res_Email);
		email_Arr[i] = res_Email;

		String res_Name = res_jsn.getString("[" + i + "].name");
		System.out.println(res_Name);
		name_Arr[i] = res_Name;

		
	System.out.println("-data from fetched data arrays to validate with expected array-");
	System.out.println(id_Arr[i]);
	System.out.println(email_Arr[i]);
	System.out.println(name_Arr[i]);
	

	Assert.assertEquals(id[i], id_Arr[i]);
	Assert.assertEquals(email[i], email_Arr[i]);
	Assert.assertEquals(name[i], name_Arr[i]);

	Assert.assertEquals(statusCode, 200);
}}}}