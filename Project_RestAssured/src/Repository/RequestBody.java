package Repository;

import java.io.IOException;
import java.util.ArrayList;
import Common_Methods.Utility;
public class RequestBody extends Environment {

	public static String req_testcase1(String TestCaseName) throws IOException {
		ArrayList<String> Data=Utility.readExcelData("POST_API", TestCaseName);
		String key_firstname= Data.get(1);
		String value_firstname= Data.get(2);
		String key_lastname= Data.get(3);
		String value_lastname = Data.get(4);

		String req_body = "{\r\n" + "    \""+key_firstname+"\": \""+value_firstname+"\",\r\n" + "    \""+key_lastname+"\": \""+value_lastname+"\"\r\n" + "}";
		return req_body;
	}
	public static String req_testcase2(String TestCaseName) throws IOException {
		ArrayList<String> Data=Utility.readExcelData("GET_API", TestCaseName);
		

		return TestCaseName;
	}


}