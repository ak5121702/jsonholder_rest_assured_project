package Common_Method;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger {

public static Response POST_trigger(String HeaderName, String HeaderValue, String reqbody, String Endpoint) {
		
		//Build the request specification using RequestSpecification class

				RequestSpecification requestSpec = RestAssured.given();

				//Request header

				requestSpec.header(HeaderName, HeaderValue);

				//Request body

				requestSpec.body(reqbody);
			
				//API request

				Response response = requestSpec.post(Endpoint);
				return response;
	}
}
