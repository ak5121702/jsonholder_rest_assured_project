package RUNNER;

import java.io.IOException;
import Common_Methods.Utility;
import Test_Cases.Get_API_TC;
import Test_Cases.Post_API_TC;
import Test_Cases.Post_API_TC_1;
import Test_Cases.Post_API_TC_2;

public class Runner {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
	    Post_API_TC.executor();
		Get_API_TC.executor();
	    Post_API_TC_1.executor();
		Post_API_TC_2.executor();
}}

